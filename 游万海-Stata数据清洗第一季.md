
&emsp;

> Stata 连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html) || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

> **温馨提示：** 定期 [清理浏览器缓存](http://www.xitongzhijia.net/xtjc/20170510/97450.html)，可以获得最佳浏览体验。

&emsp;

&emsp;

> 连享会直播课：Stata 数据清洗之实战操作（第一季）   
> **New！** 课程第二季已发布，[前往查看](https://gitee.com/arlionn/dataclean)

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201104105424.png)

&emsp; 


---

**目录**
[[TOC]]

---

&emsp;


## 1. 课程引导

**数据清洗**是论文写作过程中一个非常重要的步骤。不夸张地讲，完成一篇论文可能 70% 的时间都花在数据清洗上。

或许有人会说利用 `Excel` 也可以进行数据处理，如：取对数、求平均值等，但是对于一些复杂的操作，利用 `Excel`进行处理往往很困难，而且可重复性非常差。

大家或许都有这样的经历，论文初稿写作时利用 `Excel` 处理好数据 (\^~^ 当时也觉得过程很清楚)。当论文返修时，审稿专家让更新数据或增加变量时，回头再看之前的数据处理过程经常一头雾水，工作量往往不亚于论文初稿写作时的数据处理。

**例如**：图 1 所示数据为**多个国家**、**多年**数据，同时包含了**多个指标**。如何快速将其整理为图 2 所示面板数据形式

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/数据处理-图1-wanhai.png)


**（图1）**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/数据处理-图2-wanhai.png)

**（图2）**

以往通常的做法是将每个指标另存为一个单独的 `Excel` 文件或者下载时就将每个指标单独下载，进而对每个数据文件进行处理，最后合并到一个文件中。然而，当需要使用的变量较多时，往往显得非常繁琐。

如果有这样的一个工具，当变换原始数据时，其他步骤只需要点击一个按钮就可以进行简单的重复，那么就可以做到事半功倍，这就是我们此次课程的目的。

`Stata` 为我们提供了这样一种工具，其在数据处理、模型估计与结果导出等方面功能非常丰富。

本次课程利用 `Stata` 对论文写作中常见的数据处理操作进行讲解，并主要以案例形式进行讲解，包括：

**(1)** 以世界银行数据库 `WDI` 为例，讲解如何快速将其整理为标准的面板数据格式;

**(2)** 利用多种方法进行单位根检验，并快速的输出结果；

**(3)** 编写结果输出函数，整理为带星号的形式。 

上述案例将涉及到数据的横向合并和纵向追加、缺失值的处理、暂元、循环、`postfile` 使用及一些文字变量的处理。

本次课程遵循"**以小见大**"原则，通过实例讲解，能够覆盖绝大部分的常规数据处理方法。本讲例子主要展示跨国研究的数据处理方法，所涉及方法同样也适用于**省份和城市层面**的研究。此外，也可用于**国泰安数据库和万德数据库**等主要数据库的数据处理。

&emsp;

---
## 2. 课程详情

###  嘉宾简介

**游万海**，管理学博士，福州大学经济与管理学院教师，主要研究领域为空间计量模型、分位数回归模型及应用，以在  World Development, Energy Economics, Economics Letters, Journal of Cleaner Product， 统计研究等期刊发表 30 余篇论文。担任 Energy Economics, Economic Modelling 等期刊匿名审稿人。游老师讲授的[「文本分析与爬虫专题」](https://lianxh.duanshu.com/#/brief/course/8a9ff21a262148a1b1338769328faa6a) 受到了学员的一致好评。 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/游万海工作照.jpeg)

### 内容提要

- 不同格式数据文件的列举、重命名与合并
- `Excel` 文件中多个 `sheet` 文件操作
- 变量列举与批量重命名
- 局部宏与全局宏 (`local` and `global`)
- 常用的宏扩展函数
- `Stata` 中循环语句 (`forvalues`, `foreach`)
- 横向合并与纵向追加、关键词的统一
- 案例讲解

### 课程特色

- 短小精悍：通过案例讲解掌握常用的数据清洗方法。
- 讲义程序：分享电子版课件（数据和程序），课程中的数据清洗方法可以应用于自己的论文中。
- 课后答疑：课程结束后，授课老师将提供问题解答。

&emsp;


--- - --

## 3. 报名和咨询

- **时间**：已上线，网络直播，可随时购买
- **费用**：88 元
- **课程咨询：** 李老师-18636102467（微信同号）
- **课程主页：** <https://gitee.com/arlionn/dataclean>
- **报名主页**：&#x261D; [-点我报名-](https://www.lianxh.cn/news/f785de82434c1.html)，亦可扫描下方二维码报名  
 
  ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/游万海-数据清洗-短书二维码.png)

&emsp;

- 扫码支付 (88 元)   
  ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020暑期支付二维码180.png)

> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给李老师-18636102467（微信同号）
    


&emsp;

## 相关课程

&emsp;

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  

&emsp;

---
### 课程一览   

> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)** &#x2B50;|  | DSGE, 因果推断, 空间计量等  |
| &#x2B55; **[Stata数据清洗](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html)，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |

> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F;  连享会学习群-常见问题解答汇总：  
> &#x2728;  <https://gitee.com/arlionn/WD>  




